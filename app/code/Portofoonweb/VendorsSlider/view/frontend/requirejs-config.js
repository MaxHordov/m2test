var config = {
    map: {
        '*': {
            'slick': "Portofoonweb_VendorsSlider/js/slick"
        }
    },
    shim: {
        'slick': {
            deps: ['jquery']
        }
    }
};
