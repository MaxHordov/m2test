<?php
/**
 * Portofoonweb Theme
 *
 * @category  Portofoonweb
 * @package   Portofoonweb\Default
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Portofoonweb_VendorsSlider',
    __DIR__
);
