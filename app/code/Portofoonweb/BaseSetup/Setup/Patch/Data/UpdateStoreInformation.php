<?php

namespace Portofoonweb\BaseSetup\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;


/**
 * Class UpdateStoreInformation
 */
class UpdateStoreInformation implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var WriterInterface
     */
    private $writer;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param WriterInterface $writer
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        WriterInterface $writer
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->writer = $writer;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();

        $this->writer->save('general/locale/code','nl_NL');
        $this->writer->save('general/country/default','NL');
        $this->writer->save('general/store_information/country_id','NL');

        $this->writer->save('general/store_information/phone','0512-745293');
        $this->writer->save('general/store_information/hours','Bereikbaar maandag t/m vrijdag van: 09:00 – 17:00');
        $this->writer->save('general/store_information/name','PortofoonWeb');
        $this->writer->save('general/store_information/street_line1','Zonnedauw 16');
        $this->writer->save('general/store_information/street_line2','9202 PA Drachten');

        $this->writer->save('general/store_tax_information/kvk','62061712');
        $this->writer->save('general/store_tax_information/btw','NL854624624B01');

        $this->writer->save('trans_email/ident_support/email','info@portofoonweb.nl');

        $this->moduleDataSetup->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
