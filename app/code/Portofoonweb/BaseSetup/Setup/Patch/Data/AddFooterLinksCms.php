<?php

/**
 * Portofoonweb adding footer links cms block.
 *
 * @author Maksym Hordov <gordovmax@gmail.com>
 */

namespace Portofoonweb\BaseSetup\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Cms\Api\Data\BlockInterfaceFactory as BlockFactory;
use Magento\Cms\Api\BlockRepositoryInterface;
use Psr\Log\LoggerInterface;
use Magento\Store\Model\Store;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Exception;


class AddFooterLinksCms implements DataPatchInterface
{
    /**
     * @var BlockRepositoryInterface
     */
    private $repository;

    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * AddFooterLinksCms constructor.
     *
     * @param BlockRepositoryInterface $repository
     * @param BlockFactory $blockFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        BlockRepositoryInterface $repository,
        BlockFactory $blockFactory,
        LoggerInterface $logger
    ) {
        $this->repository = $repository;
        $this->blockFactory = $blockFactory;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $blocksContent ='<ur class="footerLinks">
                            <li class="footerTitle">KLANTENSERVICE</li>
                            <li class="footerLinks-item"><a href="#" class="footerLinks-link">Contact</a></li>
                            <li class="footerLinks-item"><a href="#" class="footerLinks-link">Over PortofoonWEB</a></li>
                            <li class="footerLinks-item"><a href="#" class="footerLinks-link">Offerte aanvraag</a></li>
                            <li class="footerLinks-item"><a href="#" class="footerLinks-link">Defect en Retour</a></li>
                            <li class="footerLinks-item"><a href="#" class="footerLinks-link">Garantie</a></li>
                            <li class="footerLinks-item"><a href="#" class="footerLinks-link">Veelgestelde vragen</a></li>
                            <li class="footerLinks-item"><a href="#" class="footerLinks-link">B2B</a></li>
                        </ur>
                        <ur class="footerLinks">
                            <li class="footerTitle">ALGEMENE INFORMATIE</li>
                            <li class="footerLinks-item"><a href="#" class="footerLinks-link">Portofoons</a></li>
                            <li class="footerLinks-item"><a href="#" class="footerLinks-link">Toepassingen</a></li>
                            <li class="footerLinks-item"><a href="#" class="footerLinks-link">Algemene voorwaarden</a></li>
                            <li class="footerLinks-item"><a href="#" class="footerLinks-link">Betalen en Verzenden</a></li>
                            <li class="footerLinks-item"><a href="#" class="footerLinks-link">Privacy en Cookies</a></li>
                            <li class="footerLinks-item"><a href="#" class="footerLinks-link">Disclaimer</a></li>
                        </ur>';

        $defaultAttributes = [
            'title' => 'Footer links',
            'identifier' => 'footer_links',
            'content' => $blocksContent,
            'is_active' => Boolean::VALUE_YES,
            'stores' => [Store::DEFAULT_STORE_ID],
        ];

        $block = $this->blockFactory->create();
        $block->setData($defaultAttributes);

        try {
            $this->repository->save($block);
        } catch (Exception $e) {
            $this->logger->error($e);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
