<?php

/**
 * Portofoonweb adding vendors slider cms block.
 *
 */

namespace Portofoonweb\BaseSetup\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Cms\Api\Data\BlockInterfaceFactory as BlockFactory;
use Magento\Cms\Api\BlockRepositoryInterface;
use Psr\Log\LoggerInterface;
use Magento\Store\Model\Store;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Exception;


class AddVendorsSliderCms implements DataPatchInterface
{
    /**
     * @var BlockRepositoryInterface
     */
    private $repository;

    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * AddVendorsSliderCms constructor.
     *
     * @param BlockRepositoryInterface $repository
     * @param BlockFactory $blockFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        BlockRepositoryInterface $repository,
        BlockFactory $blockFactory,
        LoggerInterface $logger
    ) {
        $this->repository = $repository;
        $this->blockFactory = $blockFactory;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $blocksContent ='<ul class="vendorsSlider">
                            <li class="vendorsSlier-item"><a href="{{store url="vendorspage/page/view"}}}"><img src="{{view url="images/brandLogos/brand-logo-1.png"}}" alt="logo-1" /></a></li>
                            <li class="vendorsSlier-item"><a href="{{store url="vendorspage/page/view"}}}"><img src="{{view url="images/brandLogos/brand-logo-2.png"}}" alt="logo-2" /></a></li>
                            <li class="vendorsSlier-item"><a href="{{store url="vendorspage/page/view"}}}"><img src="{{view url="images/brandLogos/brand-logo-3.png"}}" alt="logo-3" /></a></li>
                            <li class="vendorsSlier-item"><a href="{{store url="vendorspage/page/view"}}}"><img src="{{view url="images/brandLogos/brand-logo-4.png"}}" alt="logo-4" /></a></li>
                            <li class="vendorsSlier-item"><a href="{{store url="vendorspage/page/view"}}}"><img src="{{view url="images/brandLogos/brand-logo-5.png"}}" alt="logo-5" /></a></li>
                            <li class="vendorsSlier-item"><a href="{{store url="vendorspage/page/view"}}}"><img src="{{view url="images/brandLogos/brand-logo-4.png"}}" alt="logo-4" /></a></li>
                        </ul>';

        $defaultAttributes = [
            'title' => 'Vendor-logo slider',
            'identifier' => 'logo-slider',
            'content' => $blocksContent,
            'is_active' => Boolean::VALUE_YES,
            'stores' => [Store::DEFAULT_STORE_ID],
        ];

        $block = $this->blockFactory->create();
        $block->setData($defaultAttributes);

        try {
            $this->repository->save($block);
        } catch (Exception $e) {
            $this->logger->error($e);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
