<?php

/**
 * Portofoonweb adding social links cms home block.
 *
 * @author Maksym Hordov <gordovmax@gmail.com>
 */

namespace Portofoonweb\BaseSetup\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Cms\Api\Data\BlockInterfaceFactory as BlockFactory;
use Magento\Cms\Api\BlockRepositoryInterface;
use Psr\Log\LoggerInterface;
use Magento\Store\Model\Store;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Exception;


class AddSocialLinksCmsHomeBlock implements DataPatchInterface
{
    /**
     * @var BlockRepositoryInterface
     */
    private $repository;

    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * AddGratitudeCmsBlock constructor.
     *
     * @param BlockRepositoryInterface $repository
     * @param BlockFactory $blockFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        BlockRepositoryInterface $repository,
        BlockFactory $blockFactory,
        LoggerInterface $logger
    ) {
        $this->repository = $repository;
        $this->blockFactory = $blockFactory;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $blocksContent ='<div class="socialLinks-cms-home">
                            <span class="pw-logo"></span>
                            <ul>
                                <li><a href="#" class="fb" title="Portofoonweb facebook">facebook</a></li>
                                <li><a href="#" class="tw" title="Portofoonweb twitter">twitter</a></li>
                            </ul>
                        </div>';

        $defaultAttributes = [
            'title' => 'Social links home block',
            'identifier' => 'social-links-home-block',
            'content' => $blocksContent,
            'is_active' => Boolean::VALUE_YES,
            'stores' => [Store::DEFAULT_STORE_ID],
        ];

        $block = $this->blockFactory->create();
        $block->setData($defaultAttributes);

        try {
            $this->repository->save($block);
        } catch (Exception $e) {
            $this->logger->error($e);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
