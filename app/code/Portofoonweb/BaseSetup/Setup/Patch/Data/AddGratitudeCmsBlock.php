<?php

/**
 * Portofoonweb adding gratitude cms block.
 *
 * @author Maksym Hordov <gordovmax@gmail.com>
 */

namespace Portofoonweb\BaseSetup\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Cms\Api\Data\BlockInterfaceFactory as BlockFactory;
use Magento\Cms\Api\BlockRepositoryInterface;
use Psr\Log\LoggerInterface;
use Magento\Store\Model\Store;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Exception;


class AddGratitudeCmsBlock implements DataPatchInterface
{
    /**
     * @var BlockRepositoryInterface
     */
    private $repository;

    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * AddGratitudeCmsBlock constructor.
     *
     * @param BlockRepositoryInterface $repository
     * @param BlockFactory $blockFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        BlockRepositoryInterface $repository,
        BlockFactory $blockFactory,
        LoggerInterface $logger
    ) {
        $this->repository = $repository;
        $this->blockFactory = $blockFactory;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $blocksContent ='<div class="gratitudeBlock">
<h2 class="gratitudeBlock-title">Bedankt voor uw bestelling bij PortofoonWEB!</h2>
<div class="gratitudeBlock-content">
<h3 class="gratitudeBlock-subTitle">Goed om te weten</h3>
<p class="gratitudeBlock-text">Bent u tevreden over het bestelproces like dan onze Facebook pagina of laat een review achter, zo maakt u ook nog eens kans op een leuke cadeau bon ter waarde van € 100,- Wilt u graag op de hoogte blijven van het laatste nieuws en speciale aanbiedingen meld u zich dan aan op onze nieuwsbrief.</p>
<h3 class="gratitudeBlock-subTitle">Wat gaat er nu gebeuren?</h3>
<p class="gratitudeBlock-text">Per email heeft u een bevestiging ontvangen van uw bestelling met Ordernummer [ordernummer]. Heeft u de bestelling op een werkdag voor 17:00 gedaan en betaald, dan gaan wij nu direct aan de slag om te zorgen dat u het morgen in huis heeft. Heeft u vragen over de bestelling neem dan contact met ons op 0512 – 745293 of stuur ons een email aan info@portofoonweb.nl.</p>
<h3 class="gratitudeBlock-subTitle">Wist u dat?</h3>
<p class="gratitudeBlock-text">PortofoonWEB heeft sinds kort een fenomenaal nieuwe techniek genaamd Smart Portofoons. Een portofoon systeem met ongekende mogelijkheden en onbeperkt bereik! Klik op deze link voor meer informatie.</p></div>
</div>';

        $defaultAttributes = [
            'title' => 'Gratitude block',
            'identifier' => 'gratitude-block',
            'content' => $blocksContent,
            'is_active' => Boolean::VALUE_YES,
            'stores' => [Store::DEFAULT_STORE_ID],
        ];

        $block = $this->blockFactory->create();
        $block->setData($defaultAttributes);

        try {
            $this->repository->save($block);
        } catch (Exception $e) {
            $this->logger->error($e);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
