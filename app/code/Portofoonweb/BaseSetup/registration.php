<?php
/**
 * Portofoonweb Setup config
 *
 * @category  Portofoonweb
 * @package   Portofoonweb\Default
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Portofoonweb_BaseSetup',
    __DIR__
);
