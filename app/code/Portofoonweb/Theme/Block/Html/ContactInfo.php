<?php
/**
 * Portofoonweb contact info.
 */

declare(strict_types=1);

namespace Portofoonweb\Theme\Block\Html;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\ScopeInterface;

/**
 * Class HeaderContacts block.
 */
class ContactInfo extends Template
{
    /**
     * Scope Config Interface
     *
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * ContactInfo constructor.
     *
     * @param Context              $context
     * @param ScopeConfigInterface $scopeConfig
     * @param array                $data
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    /**
     * Get store phone number.
     *
     * @return mixed
     */
    public function getStorePhone()
    {
        return $this->scopeConfig->getValue(
            'general/store_information/phone',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get store work hours.
     *
     * @return mixed
     */
    public function getStoreHours()
    {
        return $this->scopeConfig->getValue(
            'general/store_information/hours',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get store name.
     *
     * @return mixed
     */
    public function getStoreName()
    {
        return $this->scopeConfig->getValue(
            'general/store_information/name',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get store street lane 1.
     *
     * @return mixed
     */
    public function getStoreStreetLane1()
    {
        return $this->scopeConfig->getValue(
            'general/store_information/street_line1',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get store street lane 2.
     *
     * @return mixed
     */
    public function getStoreStreetLane2()
    {
        return $this->scopeConfig->getValue(
            'general/store_information/street_line2',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get store email.
     *
     * @return mixed
     */
    public function getStoreEmail()
    {
        return $this->scopeConfig->getValue(
            'trans_email/ident_support/email',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get store kvk number.
     *
     * @return mixed
     */
    public function getStoreKvkNumber()
    {
        return $this->scopeConfig->getValue(
            'general/store_tax_information/kvk',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get store btw number.
     *
     * @return mixed
     */
    public function getStoreBtwNumber()
    {
        return $this->scopeConfig->getValue(
            'general/store_tax_information/btw',
            ScopeInterface::SCOPE_STORE
        );
    }
}
