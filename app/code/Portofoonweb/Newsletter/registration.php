<?php
/**
 * Portofoonweb newsletter
 *
 * @category  Portofoonweb
 * @package   Portofoonweb\Default
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Portofoonweb_Newsletter',
    __DIR__
);
