<?php
/**
 * Portofoonweb registration file theme
 *
 * @category  Portofoonweb
 * @package   Portofoonweb\Default
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::THEME,
    'frontend/Portofoonweb/default',
    __DIR__
);
